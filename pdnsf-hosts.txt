# [personalDNSFilter 1.50.49.1]
# Title: NgòBlock Domains
# Version: 202311112227
# Expires: 2 days
# Description: This list is fully bias and created for counterattack ongoing protests, if you think you're standing and/or supporting to that side, simply walk away from here and/or unsubscribe this list right now and do nothing more!
#              - Useful for uBO, uMatrix that block entire domains, however, only uBO are support wildcards.
#              - for pDNSF, please add this as additional hosts instead in order to support wildcards and domain-wide blocking.

# [Wildcards]
amnesty.*
aday*.*
bbc.*
dailymail.*
khaosod*.*
khaosod.*
lazada.*
matichon*.*
monosor.*
thairath*.*
the*.co

# Fediverse domains
kolektiva.social
mastodon.in.th
miraiverse.xyz
mstdn.in.th
pleroma.in.th
social.bbc
titipat.net
veer66.rocks
wheretogo.in.th

# [Domain-wide]
# Ngo / bad (marketing|news|press) / defame (country|monarch) / dis(credit|respect) / foundation / journalism / university / ...
# Also who support "THE (SHIT) PEOPLE AWARDS 2023!"
# Example: https://nitter.net/i/status/1435157646227673089
112watch.org
accesstrade.in.th
adaybulletin.com
adaymagazine.com
agatethai.com
amarintv.com
angkriz.com
aommoney.com
aseanmp.org
bbci.co.uk
beartai.com
bild.de
boell.de
brandthink.me
brighttv.co.th
care.or.th
catdumb.com
cmu.ac.th
coconuts.co
dailynews.co.th
decode.plus
dindeng.com
ejan.co
fccthai.com
fidh.org
frontlinedefenders.org
hrw.org
ilaw.or.th
iphonemod.net
kapook.com
khaosod.co.th
khaosodenglish.com
lazada.co.th
lazada.com
mainstand.co.th
mangozero.com
marketeeronline.co
marketingoops.com
matichon.co.th
matichonacademy.com
matichonbook.com
matichonelibrary.com
matichonweekly.com
mekongict.org
mirror.or.th
ned.org
news1live.com
newsweek.com
no112.org
ophtus.com
pixelhelper.org
pokpong.org
prachachat.net
prachatai.com
progressivemovement.in.th
project-syndicate.org
sameskybooks.net
scmp.com
sentangsedtee.com
silpa-mag.com
spaceth.co
spokedark.tv
tcijthai.com
technologychaoban.com
teroasia.com
thaienquirer.com
thaijobsgov.com
thaiking.info
thainetizen.org
thainewsonline.co
thairath.co.th
thalay.eu
the101.world
theactive.net
theisaanrecord.co
thejournalistclub.com
thematter.co
themomentum.co
thepeople.co
thereporters.co
thestandard.co
thetopics.co
tlhr2014.com
tojo.news
tu.ac.th
vice.com
voicetv.co.th
waymagazine.org
wevis.info
workpointnews.com
workpointtoday.com
zen-dai.org
#
# - Nation Broadcasting after post-2020 turning point!
bangkokbiznews.com
bangkokpost.co.th
komchadluek.net
nationtv.tv
nationweekend.com
springnews.co.th
thansettakij.com
thenation.com
thenationonlineeng.net
tnews.co.th
#
# - Thai PBS
#   * I don't believe this is a government-funded (by tax of tobacco & liquor) TV station, they mostly promoted and/or supported
#     ongoing protests and all of their relateds, but not a big surprise if you remember how this TV station start from...
#     Timeline: iTV > TITV > ThaiPBS
altv.tv
thaipbs.or.th
thaipbsworld.com
#
# - Wongnai company
#   * Big name of BlogNone writers are..., well...
#     https://nitter.net/i/status/1318856040973180929
#     https://nitter.net/i/status/1319074638480113665
#     https://nitter.net/i/status/1409077798841450497
blognone.com
brandinside.asia
wongnai.com
#
# - Continuous GMM and TV3 sanction!
#   * Some domains may already blocked due to use as AOC / CPA and their domains still operating...
atimemedia.com
bectero.com
becteroradio.com
ch3plus.com
efm.fm
g-mm.co
gmm-tv.com
gmmdigital.com
gmmgrammy.com
gmmwireless.com
greenwave.fm
oned.net
thaitv3.com

# [Toxic platform]
clubhouse.com
clubhouse.io
joinclubhouse.com
lazada.co.th
lazada.com

# [False demographic]
# Added due to I've seen some of sites that appear in 'Thailand' topic on 'GitHub' that hosted codes of sites for spread the
# one-side only truth via infographics, of course it might support ongoing protest groups.
# * It's noticeable by fonts that their use, it's often appear in online news site that absolutely support those protrest groups,
#   such as 'TheMatter', 'TheStandard', 'TheMomentum', etc.
elect.in.th
howlonguntilprayuthleaves.com

# [Broken mind devs]
# For nitter username link (not `/i/status/`), remove `/` behind divided first letter.
# https://nitter.net/h/eyfirst_
heyfirst.co
#
# https://nitter.net/i/amapinan
iotech.co.th
#
# https://nitter.net/k/laikong
klaikong.in.th
#
# https://nitter.net/i/status/1398509313493143552
# https://nitter.net/i/status/1434210080065421317
# https://nitter.net/n/arze
# As well as parodies to make some hate who stand agains them...
monosor.com
monosor.dev
narze.github.io
watasalim.vercel.app
#
ruchikachorn.com
#
# https://nitter.net/i/status/1284866197876404224
# https://nitter.net/s/rakrn
srakrn.me
#
# Disrespect meme based-on brainless game
popyut.click
prayut.click

# [Cutoff for staging with your previous custom config] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #